import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-f1',
  templateUrl: './f1.component.html',
  styleUrls: ['./f1.component.css']
})
export class F1Component implements OnInit {

  constructor(private _authService: AuthService) { }

  tokenTimer: any
  isReturn: any

  ngOnInit(): void {
    //localStorage.setItem('duration', "25")
    this.count();
  }

  count(){
    this.isReturn = setInterval(() =>{

      const duration = localStorage.getItem('duration');
      console.log("Setting timer: " + duration);
      this.tokenTimer = Number(duration) -1;
        localStorage.setItem('duration', this.tokenTimer)
  
        if (this.tokenTimer== 0  ) {
          clearInterval(this.isReturn)
          this._authService.logout();
        }
      }, 1000
    );
  }

}
