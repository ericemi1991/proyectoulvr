import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { IUser } from '../models/user';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

const BACKEND_URL = "http://localhost:3000/";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isAuthenticated: boolean = false;
  private token: string | null | undefined;
  private tokenTimer: any;
  private userId: string | null | undefined;
  private authStatusListener = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token;
  }

  getUserId() {
    return this.userId;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getExpiration() {
    return localStorage.getItem('duration');
  }


  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  //TODO: build signup request for new users

  login(user:IUser) {
  
    this.http.post(BACKEND_URL + "login", user).subscribe({
        next: (resp: any) => {
          this.processUserData(resp);
          this.router.navigate(["/f1"]);
        },
        error: (e: any) => {
          console.error(e)
          this.authStatusListener.next(false);
        },
        complete: () => console.info('Http request complete!')
        })
  }

 /*
      login(email: string, password: string) {
    const authData = { email: email, password: password };

    // this is the recommend way to subscribe
      // https://rxjs.dev/deprecations/subscribe-arguments
    this.http.post(BACKEND_URL + "/login", authData).subscribe({
        next: (resp: any) => {
          this.processUserData(resp);
          this.router.navigate(["/f1"]);

        },
        error: (e: any) => {
          console.error(e)
          this.authStatusListener.next(false);
        },
        complete: () => console.info('Http request complete!')
        })
  }

    login(user:IUser){
      this.http.post(BACKEND_URL + 'user', user).subscribe(data => {
      console.log('estoy en login', data );
      
      this.processUserData(data);
      this.router.navigate(["/f1"]);
    })
  }*/

  private processUserData(userData: any){
    this.token = userData.dataUser.accessToken;
    this.userId = userData.dataUser.id;
    const expiresInDuration = userData.dataUser.expiresIn;    
    if (this.token != null) {
      this.setAuthTimer(expiresInDuration);
      this.isAuthenticated = true;
      this.authStatusListener.next(true);
      const now = new Date();
      const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
      this.saveAuthData(this.token, expirationDate, this.userId, expiresInDuration);     
      
    }
  }

  /*isGetToken(data:any){
    const TOKEN = data.token;
    const EXPIRETOKEN = data.expiresIn;
  }*/
  //private saveAuthData(token: string, expirationDate: Date, userId: any)
  private saveAuthData(token: string, expirationDate: Date, userId: any, expiresInDuration:any) {
    localStorage.setItem("token", token);
    localStorage.setItem("expiration", expirationDate.toISOString());
    localStorage.setItem("userId", userId);
    localStorage.setItem("duration", expiresInDuration);
  }

  private getAuthData() {
    const token = localStorage.getItem("token");
    const expirationDate = localStorage.getItem("expiration");
    const userId = localStorage.getItem("userId");
    if (!token || !expirationDate) {
      return;
    }
    return {
      token: token,
      expirationDate: new Date(expirationDate),
      userId: userId
    }
  }

  autoAuthUser() {
    const authData = this.getAuthData();
    if (!authData) {
      return;
    }
    const now = new Date();
    const expiresIn = authData.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authData.token;
      this.isAuthenticated = true;
      this.userId = authData.userId;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
    }
  }

  private setAuthTimer(duration: number) {
    console.log("Setting timer: " + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    clearTimeout(this.tokenTimer);
    this.userId = null;
    this.clearAuthData();
    this.router.navigate(["/"]);
  }

  private clearAuthData() {
    localStorage.removeItem("token");
    localStorage.removeItem("expiration");
    localStorage.removeItem("userId");
  }
}
